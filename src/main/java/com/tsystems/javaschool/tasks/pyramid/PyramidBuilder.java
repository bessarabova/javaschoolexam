package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int numberRequired = 0;     //Required number of elements to build a pyramid
        int numberLines = 0;        //Total number of lines
        while (numberRequired < inputNumbers.size()) {
            //There must be so many elements in the input list
            //that you can place on each line the number of elements equal to the line number
            numberRequired += (++numberLines);
        }
        int numberColumns = numberLines * 2 - 1;        //Total number of columns
        if (numberRequired != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }
        try{
            Collections.sort(inputNumbers);
        }catch (Error e){
            throw new CannotBuildPyramidException();
        }catch(Exception ee){
            throw new CannotBuildPyramidException();
        }

        try{
            int[][] result = new int[numberLines][numberColumns];

            int countArr = 0;       //Counter to traverse input list
            int jStart = (numberColumns - 1) / 2 + (numberColumns - 1) % 2;     //Location of first element
            //Fill the result array with data from the input list
            for (int i = 0; i < numberLines; i++) {
                if (i == 0) {
                    result[i][jStart] = inputNumbers.get(countArr);
                    countArr++;
                } else {
                    int k = 0;  //Local counter
                    int j = jStart - 1;
                    while (k <= i) {
                        result[i][j] = inputNumbers.get(countArr);
                        j += 2;
                        k++;
                        countArr++;
                    }
                    jStart--;
                }
            }
            //printArray(result);
            return result;
        }catch(Error e){
            throw new CannotBuildPyramidException();
        }

    }

    private void printArray(int[][] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                System.out.printf("%4d",input[i][j]);
            }
            System.out.println();
        }
    }

}
