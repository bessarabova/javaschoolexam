package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private String operators = "+-*/";

    public String evaluate(String statement) {
        //Converting from infix notation
        List<String> input = parseIntoRPN(statement);
        if (input == null|| statement.equals(null)) {
            return null;
        }
        Stack<Double> stackOperand = new Stack<>();
        for (String token : input) {
            if (isNumber(token)) {
                try {
                    stackOperand.push(Double.parseDouble(token));
                } catch (NumberFormatException e) {
                    return null;
                }
            } else {
                if (isOperator(token)) {
                    if (stackOperand.size() >= 2) {
                        switch (token) {
                            case "+":
                                stackOperand.push(stackOperand.pop() + stackOperand.pop());
                                break;
                            case "-":
                                double subtrahend = stackOperand.pop();
                                double number =stackOperand.pop();
                                stackOperand.push( number-subtrahend );
                                break;
                            case "*":
                                stackOperand.push(stackOperand.pop() * stackOperand.pop());
                                break;
                            case "/":
                                try{
                                    double divisor = stackOperand.pop();
                                    double divisible =stackOperand.pop();
                                    if(divisor == 0){
                                        return null;
                                    }
                                    stackOperand.push( divisible/divisor );
                                }catch (ArithmeticException e){
                                    return null;
                                }
                                break;
                            default:
                                return null;
                        }
                    } else {
                        return null;
                    }

                }
            }
        }

        //Rounding
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        double result = stackOperand.peek();
        return df.format(result).replace(",",".");

    }


    //Auxiliary methods for parseIntoRPN
    private boolean isNumber(String input) {
        try {
            Float.parseFloat(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }

    private boolean isOpenParenthesis(String input) {
        return input.equals("(");
    }

    private boolean isCloseParenthesis(String input) {
        return input.equals(")");
    }

    private boolean isOperator(String input) {
        if (input.length() != 1) {
            return false;
        } else {
            return operators.contains(input);
        }
    }

    private int getOperatorPriority(String input) {
        if (input.equals("*") || input.equals("/")) {
            return 3;
        } else {
            if (input.equals("+") || input.equals("-")) {
                return 2;
            } else {
                return 1;
            }
        }
    }

    public List<String> parseIntoRPN(String input) {
        // Check input's validation
        if(input==null || input.length()==0){
            return null;
        }
        // Preparatory actions
        input.replaceAll(" ", "");
        input.replaceAll("\\(-", "0-");
        if (input.charAt(0) == '-') {
            input = "0" + input;
        }
        // Split into elements
        StringTokenizer tokenizer = new StringTokenizer(input, operators + "()", true);

        Stack<String> stackOperations = new Stack<String>();
        List<String> result = new ArrayList();

        // Algorithm for reducing to reverse polish notation
        while (tokenizer.hasMoreTokens()) {
            boolean flag = false;
            String token = tokenizer.nextToken();
            //System.out.println("Проверяем token: " + token);
            if (isNumber(token)) {
                // System.out.println("is number");
                result.add(token);
                flag = true;
            }
            if (isOpenParenthesis(token)) {
                //System.out.println("is open parenthesis");
                stackOperations.push(token);
                flag = true;
            }
            if (isCloseParenthesis(token)) {
                //System.out.println("is close parenthesis");
                while (!stackOperations.isEmpty() && !isOpenParenthesis(stackOperations.peek())) {
                    result.add(stackOperations.pop());
                }
                try{
                    stackOperations.pop();
                }catch (EmptyStackException e){
                    return null;
                }
                flag = true;
            }
            if (isOperator(token)) {
                //System.out.println("is operator");
                while (!stackOperations.isEmpty() && getOperatorPriority(stackOperations.peek()) >= getOperatorPriority(token)) {
                    result.add(stackOperations.pop());
                }
                stackOperations.push(token);
                flag = true;
            }
            //System.out.println("result: " + result + ";  stack: " + stackOperations);
            if (!flag) {
                return null;
            }

        }

        while (!stackOperations.isEmpty()) {
            if (!isOperator(stackOperations.peek())) {
                return null;
            } else {
                result.add(stackOperations.pop());
            }
        }

        return result;
    }

}
